package edu.ec.istdab.dao;

import java.util.List;

import edu.ec.istdab.model.Persona;

public interface ICrud<T> {
	Integer registrar(T t) throws Exception;
	Integer modificar(T t) throws Exception;
	Integer eliminar(T t) throws Exception;
	List<T> listar() throws Exception;
}
