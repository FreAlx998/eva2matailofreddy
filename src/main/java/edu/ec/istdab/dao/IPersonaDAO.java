package edu.ec.istdab.dao;

import java.util.List;

import javax.ejb.Local;

import edu.ec.istdab.model.Persona;

//en contexto de la misma maquina virtual
@Local
public interface IPersonaDAO extends ICrud<Persona>{
	//La interface ICRUD realiza todos los procesos para registro de Personas
}
