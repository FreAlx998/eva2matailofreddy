package edu.ec.istdab.controlador;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import edu.ec.istdab.dao.IPersonaDAO;
import edu.ec.istdab.model.Persona;

@Named
@ViewScoped
public class PersonaBean implements Serializable{
	
	@Inject
	private IPersonaDAO dao;
	
	private Persona persona;
	private String tipoDialogo;
	private List<Persona> lista;
	
	public void operar(String accion) {
		try {
			if (accion.equalsIgnoreCase("R")) {
				this.dao.registrar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso: ", "Registro completado con exito"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
		public void mostrarDatos(Persona p) {
			this.persona = p;
			this.tipoDialogo = "Modificar Persona";
		}
		
		public void listar() {
			try {
				this.lista = this.dao.listar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	
}
