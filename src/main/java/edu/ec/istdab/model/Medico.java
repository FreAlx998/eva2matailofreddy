package edu.ec.istdab.model;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Named
@Table(name = "medico")
public class Medico implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idmedico;
	
	@Column(name = "nombre",nullable = false,length = 25)
	private String nombre;
	
	@Column(name = "apellido",nullable = false,length = 25)
	private String apellido;
}
