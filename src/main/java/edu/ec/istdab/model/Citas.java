package edu.ec.istdab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "citas")
public class Citas implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idcita;
	
	@ManyToOne
	@JoinColumn(name = "idmedico", nullable = false)
	private Medico idmedico;
	
	@ManyToOne
	@JoinColumn(name = "idmascota", nullable = false)
	private Mascota idmascota;
	
	@ManyToOne
	@JoinColumn(name = "idPersona", nullable = false)
	private Persona idpersona;
}
