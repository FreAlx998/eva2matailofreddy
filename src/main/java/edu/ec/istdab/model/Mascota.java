package edu.ec.istdab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascota")
public class Mascota implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idmascota;
	
	@Column(name = "nombre",nullable=false,length=25)
	private String nombre;
	
	@Column(name = "raza",nullable=false,length=25)
	private String raza;
	
	@Column(name = "sexo",nullable=false,length=25)
	private String sexo;
	
	@Column(name = "edad",nullable=false,length=25)
	private String edad;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public Integer getIdmascota() {
		return idmascota;
	}
	public void setIdmascota(Integer idmascota) {
		this.idmascota = idmascota;
	}
}
